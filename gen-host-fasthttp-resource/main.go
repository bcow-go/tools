package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/printer"
	"go/token"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"unicode"

	"golang.org/x/mod/modfile"
	"golang.org/x/tools/go/ast/astutil"
)

const (
	RESOURCE_MANAGER_TYPE_NAME string = "ResourceManager"
	RESOURCE_SUBMODULE_NAME    string = "resource"
	RESOURCE_TYPE_SUFFIX       string = "Resource"

	RESOURCE_FILE_TEMPLATE string = `package resource

import (
	"github.com/valyala/fasthttp"
	"gitlab.bcowtech.de/bcow-go/host-fasthttp/response"
)

type %[1]s struct {}

func (r *%[1]s) Ping(ctx *fasthttp.RequestCtx) {
	response.Success(ctx, "text/plain", []byte("PONG"))
}
`
)

var (
	gofile string
)

func main() {
	if gofile == "" {
		gofile = os.Getenv("GOFILE")
		if gofile == "" {
			abort("No file to parse.")
		}
	}

	fset := token.NewFileSet()
	f, err := parser.ParseFile(fset, gofile, nil, parser.ParseComments)
	if err != nil {
		panic(err)
	}

	for _, node := range f.Decls {
		switch node.(type) {

		case *ast.GenDecl:
			genDecl := node.(*ast.GenDecl)
			for _, spec := range genDecl.Specs {
				switch spec.(type) {
				case *ast.TypeSpec:
					typeSpec := spec.(*ast.TypeSpec)

					structTypeName := typeSpec.Name.Name
					if structTypeName == RESOURCE_MANAGER_TYPE_NAME {
						switch typeSpec.Type.(type) {
						case *ast.StructType:
							structType := typeSpec.Type.(*ast.StructType)
							err := generateResourceFiles(structType, RESOURCE_SUBMODULE_NAME)
							if err != nil {
								abort(err.Error())
							}
						}

						// import resource module path
						err := importResourceModulePath(fset, f)
						if err != nil {
							abort(err.Error())
						}
						break
					}
				}
			}
		}
	}
}

func abort(err string) {
	fmt.Fprintln(os.Stderr, err)
	os.Exit(1)
}

func generateResourceFiles(structType *ast.StructType, resourceDir string) error {
	if _, err := os.Stat(resourceDir); os.IsNotExist(err) {
		os.Mkdir(resourceDir, os.ModePerm)
	}

	for _, field := range structType.Fields.List {
		switch field.Type.(type) {
		case *ast.StarExpr:
			star := field.Type.(*ast.StarExpr)
			i, ok := star.X.(*ast.Ident)
			if ok {
				fieldType := i.Name

				err := writeResourceFile(fieldType, resourceDir)
				if err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func writeResourceFile(typename string, resourceDir string) error {
	if strings.HasSuffix(typename, RESOURCE_TYPE_SUFFIX) {
		for i, ch := range typename {
			if unicode.IsUpper(ch) && unicode.IsLetter(ch) {
				// convert the type name to filename
				filename := string(unicode.ToLower(ch)) + typename[i+1:]
				path := filepath.Join(resourceDir, filename+".go")

				if _, err := os.Stat(path); os.IsNotExist(err) {
					f, err := os.Create(path)
					if err != nil {
						return err
					}
					return writeResouceContent(f, typename)
				}
			}
			break
		}
	}
	return nil
}

func writeResouceContent(file *os.File, typename string) error {
	_, err := fmt.Fprintf(file,
		RESOURCE_FILE_TEMPLATE, typename)

	return err
}

func importResourceModulePath(fset *token.FileSet, f *ast.File) error {
	modulePath, err := getModulePath()
	if err != nil {
		abort(err.Error())
	}
	resourceModulePath := modulePath + "/" + RESOURCE_SUBMODULE_NAME
	ok := astutil.AddNamedImport(fset, f, ".", resourceModulePath)
	if ok {
		stream, err := os.OpenFile(gofile, os.O_WRONLY, os.ModePerm)
		if err != nil {
			return err
		}
		err = printer.Fprint(stream, fset, f)
		if err != nil {
			return err
		}
	}
	return nil
}

func getModulePath() (string, error) {
	goModBytes, err := ioutil.ReadFile("go.mod")
	if err != nil {
		return "", err
	}

	modName := modfile.ModulePath(goModBytes)

	return modName, nil
}
